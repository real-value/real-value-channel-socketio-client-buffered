# real-value-channel-socketio-client-buffered Library     

## About

This library provides a client(browser) side implementation of a channel.

A channel is an object once established which provides 3 methods.
- the `enqueue` method to push content into the channel
- the `length` method to check if there is content to retreive from the channel
- the `dequeue` method to get unseen content from the channel

The server side channel implementation keeps a buffer of length `bufferLength` such that new clients receive the buffered content.
This provides that value in that the server can retain some historical data so that when new channel clients connect they get the historical data and any subsequent new data that arrives.

## Install

```
npm install real-value-channel-socketio-client-buffered
yarn install real-value-channel-socketio-client-buffered
```

## How to use

Instantiate the channel
```
let ChannelFactory = ChannelSocketIOClient({url: "http://127.0.0.1:9000"})
let channel = ChannelFactory('production')  //A channel of production data
```

Then use the `enqueue`, `length`, `dequeue` methods as required