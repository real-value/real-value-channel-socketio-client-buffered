import assert from 'assert'
import Debug from 'debug'
const debug = Debug('lib-real-value-channel-socketio-client-buffered')

import io from 'socket.io-client'

export function ChannelFactory (options) {

    let { url } = options

    let outBuffer = []
    let inBuffer = {}

    let sending= true

    const socket = io(url);

    function startSending(socket){
        if(!sending) return;

        while(outBuffer.length>0){
            debug(`Emit Client`)
            socket.emit('Channel',outBuffer.shift())
        }
        setTimeout(()=>{startSending(socket)},500)
    }
    startSending(socket)

    socket.on('Channel',(msg)=>{
        debug(`Client Received ${msg}`)
        debug(msg)
        if(inBuffer[msg.channel]===undefined){
            inBuffer[msg.channel]=[]
        }
        inBuffer[msg.channel].push(msg.value)
    })

    return (name)=> {

        //Subscribe
        socket.emit('Channel',{channel: "subscribe",value: name})
        
        return {
            close: ()=>{sending=false;socket.close()},
            enqueue: x => {
                debug(`enqueue ${name}`)
                outBuffer.push({ channel: name, value: x })
            },
            length: () => inBuffer[name]!==undefined ? inBuffer[name].length : 0,
            dequeue: () => {
                assert(inBuffer[name] && inBuffer[name].length>0,"Ensure length is > 0 before invoking dequeue")
                let ans = inBuffer[name].shift() 
                debug(`dequeue ${name} buffer length:${inBuffer[name].length}:${JSON.stringify(ans)}`)
                return ans
            }
        }
    }
}
